# Spring-cloud (my-config-server)

Exemplo de configuração usando o spring cloud
![SpringClound](https://gitlab.com/osmarhes/spring-cloud/raw/master/images/novas/microservices-springcloud.png)

### Eureka server

Projeto que registra os microserviços e monitora o status de cada
![Eureka](https://gitlab.com/osmarhes/spring-cloud/raw/master/images/novas/eureka.png)

![EurekaDiagrama](https://gitlab.com/osmarhes/spring-cloud/raw/master/images/novas/eureka-diagrama.png)

### Config server

![ConfigServer](https://gitlab.com/osmarhes/spring-cloud/raw/master/images/novas/configserver-diagrama.png)

* Lê as configurações do github dos projetos abaixo:
  - http://localhost:9080/my-eureka/default
  - http://localhost:9080/my-customer/default
  - http://localhost:9080/my-product/default
  - http://localhost:9080/my-sale/default
  - http://localhost:9080/my-zuul/default
  

#### Zuul
  Projeto de proxy para os serviços rest
  ![Zuul](https://gitlab.com/osmarhes/spring-cloud/raw/master/images/novas/comxsem-zuul.png)


### Microservices projects

Projetos de microserviços que retorna um json via Rest

- https://gitlab.com/osmarhes/spring-cloud/tree/master/my-customer
- https://gitlab.com/osmarhes/spring-cloud/tree/master/my-product
- https://gitlab.com/osmarhes/spring-cloud/tree/master/my-sale
